#ifndef VECTOR_H
#define VECTOR_H

#include <assert.h>
#include <memory.h>
#include <stddef.h>
#include <stdint.h>

#define OFFSET(o, offset) (void *)(((char *)(o)) + (offset))

struct generic_vec {
  int32_t count;
  int32_t size;
  int32_t element_size;
};

#define VECTOR_VALUE(T, sz)                                                    \
  { .count = 0, .size = sz, .element_size = sizeof(T) }

#define VECTOR_DECLARE(T) typedef struct __##T##_vector __##T##_vector;

#define VECTOR_DEFINE(T, size)                                                 \
  struct {                                                                     \
    generic_vec header;                                                        \
    T array[size];                                                             \
  } __##T##_vector = {.header = VECTOR_VALUE(T, size)};                        \
                                                                               \
  generic_vec *vec = (generic_vec *)&__##T##_vector;

typedef struct generic_vec generic_vec;

void _vector_add(generic_vec *vec, void *element) {
  assert(vec != NULL);
  assert(element != NULL);

  if (vec->count >= vec->size) {
    // Resize
  }

  void *array =
      OFFSET(vec, sizeof(generic_vec) + (vec->count * vec->element_size));
  vec->count++;
  memcpy(array, element, vec->element_size);
}

#endif
