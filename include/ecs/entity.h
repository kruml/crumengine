#ifndef ENTITY_H_
#define ENTITY_H_

#include "ecs/types.h"

#include <stddef.h>
#include <stdint.h>

typedef struct stack_int stack_int; //** < Stack used for available entities */

/**
 * Entity is defined by an ID and a Signature for active components
 */
typedef struct {
  EntityId id;
  Signature signature; //** < Signature shows active components as a bitmask */
} Entity;

typedef struct {
  Entity *buf;
  // TODO: Maybe unnecessary to store available ids in entity struct,
  //  could be stored here in module instead.
  stack_int *available_ids;
  size_t active;
  size_t max;
} EntityList;

/* Entity functions */
EntityId EntityCreate(EntityList *entity_list);
void EntityDestroy(EntityList *entity_list, EntityId id);
void EntitySetSignature(EntityList *entity_list, EntityId entity,
                        Signature signature);
Signature EntityGetSignature(EntityList *entity_list, EntityId entity);
/* Entity list functions */
void EntityListCreate(EntityList *list);
void EntityListDestroy(EntityList *entities);

#endif // ENTITY_H_
