#ifndef COMPONENT_H
#define COMPONENT_H

#include "common/defines.h"
#include "ecs/types.h"

#include <stddef.h>
#include <stdint.h>

#define COMPONENT_SET_STARTING_SIZE (1000)
#define COMPONENT_LIST_STARTING_SIZE (100)

typedef struct SparseSet ComponentSet;

/**
 * Holds a SparseSet for components
 */
typedef struct {
  size_t active;
  size_t max;
  ComponentSet *sets[COMPONENT_SET_STARTING_SIZE];
} ComponentList;

void ComponentListCreate(ComponentList *list);

void ComponentListDestroy(ComponentList *list);

void ComponentAdd(EntityId entity_id, ComponentList *list, ComponentId id,
                  void *component, size_t component_size);

void ComponentDestroy(EntityId entity, ComponentList *list,
                      ComponentId component, size_t element_size);

void *ComponentGet(EntityId entity, ComponentList *component_list,
                   ComponentId cid, size_t component_size);

#endif // COMPONENT_H
