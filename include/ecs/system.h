#ifndef SYSTEM_H
#define SYSTEM_H

#include "decs.h"
#include "entity.h"
#include "types.h"

#include <stdint.h>
#include <stdlib.h>

// TODO: Perhaps revise double pointer. Is this the best way?
typedef struct {
  int active_systems;
  int capacity;
  System **systems;

} SystemList;

void system_list_create(SystemList *list);
void system_list_destroy(SystemList *list);
DecsStatus system_list_update(SystemList *list, float dt);

System *system_create(SystemUpdateCallback cb, Signature signature);
SystemId system_add(SystemList *list, System *system);

#endif // SYSTEM_H
