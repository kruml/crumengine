#include "ecs/component.h"
#include "ecs/types.h"

#include "common/data_structures/sparse_set.h"

#include <assert.h>
#include <stddef.h>
#include <stdlib.h>

void ComponentListCreate(ComponentList *list) {
  /* Components *list = malloc(sizeof(Components)); */
  list->active = 0;
  list->max = COMPONENT_SET_STARTING_SIZE;
  for (int i = 0; i < COMPONENT_SET_STARTING_SIZE; i++) {
    list->sets[i] = NULL;
  }
}

void ComponentListDestroy(ComponentList *list) {
  for (size_t i = 0; i < list->active; i++) {
    SparseSetDestroy(list->sets[i]);
  }
}

void component_list_register(ComponentList *list, ComponentId id,
                             size_t element_size) {
  list->sets[id] = SparseSetCreate(COMPONENT_SET_STARTING_SIZE, element_size);
  list->active++;
}

void ComponentDestroy(EntityId entity, ComponentList *list, ComponentId id,
                      size_t element_size) {
  assert(list->sets[id] != NULL);
  SparseSetRemoveElement(list->sets[id], entity, element_size);
}

void ComponentAdd(EntityId entity_id, ComponentList *list, ComponentId id,
                  void *component, size_t component_size) {
  if (list->sets[id] == NULL) {
    component_list_register(list, id, component_size);
  }
  SparseSetAddElement(list->sets[id], entity_id, component, component_size);
}

void *ComponentGet(EntityId entity, ComponentList *component_list,
                   ComponentId cid, size_t component_size) {
  return SparseSetGetElement(component_list->sets[cid], entity, component_size);
}
