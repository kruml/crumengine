#include "data_structures/linked_list.h"
#include "common/log.h"
#include "decs.h"

#include <stddef.h>
#include <stdlib.h>

KECS_STATUS node_create(node_t *new_node, size_t data_size) {

  new_node = malloc(sizeof(node_t) + data_size);
  if (new_node == NULL) {
    return KECS_FAIL;
  }

  new_node->data = NULL;
  new_node->next = NULL;

  return KECS_SUCCESS;
}

linked_list_t ll_create() {
  linked_list_t new_list = {.head = NULL, .size = 0};
  return new_list;
}

KECS_STATUS ll_add(linked_list_t *list, void *data, size_t size) {
  node_t *new_node = list->head;
  while (new_node != NULL) {
    new_node = new_node->next;
  }

  if (KECS_FAIL == node_create(new_node, size)) {
    return KECS_FAIL;
  }

  new_node->data = data;
  list->size++;

  return KECS_SUCCESS;
}
KECS_STATUS ll_remove(linked_list_t *list, size_t idx) {
  node_t *node = list->head;
  for (size_t i = 0; i < idx - 1; i++) {
    if (node->next == NULL) {
      LOG_ERROR("Index %d out of bounds for linked list!");
      return KECS_FAIL;
    }
    node = node->next;
  }
}
KECS_STATUS ll_get_data(linked_list_t *list, size_t node, void *data,
                        size_t size);
size_t linked_list_get_size(linked_list_t *list);
